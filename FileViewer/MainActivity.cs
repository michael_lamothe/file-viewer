﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Linq;
using System.IO;
using Android.Webkit;
using System.Collections.Generic;

namespace FileViewer
{
    [Activity(Label = "File Viewer", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        Stack<string> Paths { get; set; } = new Stack<string>();

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
        }

        protected override void OnResume()
        {
            base.OnResume();
            Paths.Push(System.Environment.CurrentDirectory);
            LoadCurrentDirectory();
        }

        public override void OnBackPressed()
        {
            if (Paths.Count() > 1)
            {
                Paths.Pop();
            }

            System.Environment.CurrentDirectory = Paths.Peek();
            LoadCurrentDirectory();
        }

        void LoadCurrentDirectory()
        {
            var layout = FindViewById<LinearLayout>(Resource.Id.Main);
            layout.RemoveAllViews();

            var directories = System.IO.Directory.GetDirectories(System.Environment.CurrentDirectory).ToList();
            if (System.Environment.CurrentDirectory != "/")
            {
                directories.Insert(0, "..");
            }

            Window.SetTitle(System.Environment.CurrentDirectory);

            directories.ForEach(path =>
            {
                var directoryName = Path.GetFileName(path);
                var button = new Button(this) { Text = $"[DIR] {directoryName}", Gravity = GravityFlags.Left };
                button.Click += (s, e) =>
                {
                    var previousDirectory = System.Environment.CurrentDirectory;
                    try
                    {
                        System.Environment.CurrentDirectory = path;
                        LoadCurrentDirectory();
                        Paths.Push(System.Environment.CurrentDirectory);
                    }
                    catch (UnauthorizedAccessException ex)
                    {
                        ShowMessage(ex.Message);

                        System.Environment.CurrentDirectory = previousDirectory;
                        LoadCurrentDirectory();
                    }
                };

                layout.AddView(button);
            });

            var files = System.IO.Directory.GetFiles(System.Environment.CurrentDirectory).ToList();
            files.ForEach(path =>
            {
                var fileName = Path.GetFileName(path);
                var button = new Button(this) { Text = fileName, Gravity = GravityFlags.Left };
                button.Click += (s, e) =>
                {
                    var mimeType = MimeTypeMap.Singleton.GetMimeTypeFromExtension(Path.GetExtension(fileName));
                    var intent = new Intent(Intent.ActionView);
                    intent.SetDataAndType(Android.Net.Uri.FromFile(new Java.IO.File(path)), mimeType);
                    intent.SetFlags(ActivityFlags.NewTask);
                    try
                    {
                        StartActivity(intent);
                    }
                    catch (Android.Content.ActivityNotFoundException ex)
                    {
                        ShowMessage(ex.Message);
                    }
                };
                layout.AddView(button);
            });
        }

        void ShowMessage(string message)
        {
            var builder = new AlertDialog.Builder(this);
            builder.SetMessage(message);
            builder.SetCancelable(true);
            builder.SetNegativeButton(Resource.String.Close, (dialog, id) => { });
            var alert = builder.Create();
            alert.Show();
        }
    }
}

